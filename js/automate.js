$(document).ready(function(){
    var localStorage = window.localStorage;
    localStorage.clear();
    getLocation();
    var errorsArray = [];

    function getCity(lat,long){

        console.log(lat);
        console.log(long);

        var x='https://maps.googleapis.com/maps/api/geocode/json?latlng='+ lat + ',' + long + '&sensor=true';
        var city;
        var count=0;
        var gg=$.getJSON(x,function(data)
        {
            for(var i=0;i<10;i++)
            {
                for(var j=0;j<10;j++)
                {
                    if(typeof data.results[i].address_components[j].types[0] &&  data.results[i].address_components[j].types[0]=="locality")
                    {
                        city=data.results[i].address_components[j].long_name;
                        // alert(city);
                        count=1;
                        // return city;
                        cityFound(city);
                        break;
                    }
                }
                if(count===1)
                {
                    break;
                }
            }
            
        });
        // alert(city);

    }

    function cityFound(city){

        console.log(city);
        localStorage.setItem('location',city);
    }

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        var location = "latitude =" + position.coords.latitude + " and longitude =" + position.coords.longitude;
        /*var city = getCity(position.coords.latitude,position.coords.longitude);
        console.log(city);
        localStorage.setItem("location", city);*/
        getCity(position.coords.latitude,position.coords.longitude);
    }

    function showError(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
            console.log("User denied the request for Geolocation.")
            break;
            case error.POSITION_UNAVAILABLE:
            console.log("Location information is unavailable.")
            break;
            case error.TIMEOUT:
            console.log("The request to get user location timed out.")
            break;
            case error.UNKNOWN_ERROR:
            console.log("An unknown error occurred while getting geolocation")
            break;
        }
    }

    $("#popupButton").click(function(e) {
        var otp = $("#otp").val();
        if (otp == localStorage.getItem("otp")) {      
            sendFormDataToLMS(localStorage.getItem("formSubmission"));

        } else {
            alert("otp doesnt match");
        }

    });


    $("#submitBtn").click(function(e) {
        $("#submitBtn").attr("disabled","disabled");

        e.preventDefault();

        var formSubmission = {
            name: $("#name").val(),
            email: $("#email").val(),
            mobile: $("#mobile").val(),
            pan: $("#pan").val(),
            dob: $("#dateOfBirth").val()
        }
    //getLocation();
    var panValidation = validatePanCard(formSubmission["pan"]);
    var ispanValid = panValidation.done(function(data) {
     console.log(data);
     var valid = data[6];
            if (valid == 0) //meaning pan is invalid
            {
                alert("\n Enter Valid PAN Card Number");   
            }
            else //means pan is valid
            {
                var allOkay = validateInput(formSubmission);
                if (allOkay) 
                {
                    var mobile = formSubmission["mobile"];
                    formSubmission = JSON.stringify(formSubmission);
                    localStorage.setItem("formSubmission", formSubmission);
                    sendSms(mobile);
                    $('#myModal').modal('show');
                }           
            }
            $("#submitBtn").removeAttr("disabled");

        });

});


    function getAge(born, now) {
        var birthday = new Date(now.getFullYear(), born.getMonth(), born.getDate());
        if (now >= birthday)
            return now.getFullYear() - born.getFullYear();
        else
            return now.getFullYear() - born.getFullYear() - 1;
    }

    function sendSms(mobile) {
        var otp = getOtp();
        console.log(otp);
        localStorage.setItem("otp", otp);

        $.ajax({

            url: "https://bulkpush.mytoday.com/BulkSms/SingleMsgApi?",
            
            type: "get",     //send it through get method
            data: {
                jobname: 'RELSEC',
                feedid: '365430',
                password: 'jptpa',
                UserName: '8108292646',
                senderid: 'RELSEC',
                To: mobile,
                Text: 'Thank you for showing interest in opening a trading account with Reliance Securities. Your OTP is ' + otp,


            },
            success: function(response) {
                console.log(response);
            //console.log( " success "+ otp + ' and mobile is '+mobile);
            //console.log(" Successful Request Made on " + (new Date().toLocaleString()));
        },
        error: function(xhr, err) {
            console.log("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
            console.log("Something went wrong when connecting on " +
                (new Date().toLocaleString()));
        }
    });
    }

    function getOtp() {
        return Math.floor((Math.random() * 9999) + 1000);
    }

    function sendFormDataToLMS(formSubmission) {

        // console.log('inside sendFormDataToLMS')

        formSubmission = JSON.parse(formSubmission);

        var leadName = formSubmission["name"];
        var leadMobileNumber = formSubmission["mobile"];
        leadMobileNumber = parseInt(leadMobileNumber);
        var leadEmailId = formSubmission["email"];
    // getLocation();
    var location = localStorage.getItem("location");
    console.log(location);

    $.ajax({
        // url: "http://lms.rsec.co.in/Leads/Service.svc/Rest/LeadCreation?",
        url: "https://lms.rsec.co.in/Avaamo.svc/avaamomobileapplms/CreateLead/?",
        

        type: "get",
         //send it through get method
         data: {
            /*LeadSource: 'GGS_LP',
            UserName: 'test',
            Password: 'pass@123',
            Name: leadName,
            MobileNo: leadMobileNumber,
            EmailId: leadEmailId,
            Location: 'empty',
            Remarks: 'The city of lead is ' + location*/
            FullName: leadName,
            Emailid: leadEmailId,
            MobileNo: leadMobileNumber,
            LeadSource: 'GGS_LP',
            Location: location,
            Campaign: 'HNI',
            Remarks: 'Testing lead ignore',
            Var_UserId: 'AvaamoAppUser',
            Var_Password: 'AvaamoAppP@ssw)rd'

        },
        success: function(response) {

            console.log(response);    

            var res = $(response).find("string").text();
            console.log(res);
            localStorage.setItem("confirmation-id", res);
            responseId = localStorage.getItem("confirmation-id");
            window.location = "thankyou.html?id=" + responseId;
        },
        error: function(jQXHR, textStatus, errorThrown) {
            console.log("An error occurred whilst trying to contact the server: " +
                jQXHR.status + " " + textStatus + " " + errorThrown + " on " +
                (new Date().toLocaleString()));
        }
    });
    var confirmationId = localStorage.getItem("confirmation-id");
   // console.log(confirmationId);

}

function validateInput(formSubmission) {

    var nameRegex = /^[a-zA-Z ]{2,50}$/;
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var phoneRegexIndia = /^(\+\d{1,3}[- ]?)?\d{10}$/;
    var panRegex = /^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/;

    if (formSubmission["name"].length == 0 || formSubmission["name"] == "") {
        errorsArray.push("\n Enter Full Name");
    } else {
        if (!formSubmission["name"].match(nameRegex)) {
            errorsArray.push("\n Enter Correct Full Name");
        }
    }

    if (formSubmission["email"].length == 0 || formSubmission["email"] == "") {
        errorsArray.push("\n Enter Email Address");
    } else {
        if (!formSubmission["email"].match(emailRegex)) {
            errorsArray.push("\n Enter Valid Email Address");
        }
    }

    if (formSubmission["mobile"].length == 0 || formSubmission["mobile"] == "") {
        errorsArray.push("\n Enter Mobile Number");
    } else {
        if (!formSubmission["mobile"].match(phoneRegexIndia)) {
            errorsArray.push("\n Enter Valid Phone Number");
        }
    }

    if (formSubmission["pan"].length == 0 || formSubmission["pan"] == "") {
        if($.inArray("\n Enter Valid PAN Card Number", errorsArray))    
            errorsArray.push("\n Enter PAN Card Number");
    } else {
        if (!formSubmission["pan"].match(panRegex)) {
            if($.inArray("\n Enter Valid PAN Card Number", errorsArray))    
                errorsArray.push("\n Enter Valid PAN Card Number");
        } 
    }

    if (formSubmission["dob"].length == 0 || formSubmission["dob"] == "") {
        errorsArray.push("\n Enter Date Of Birth");
    } else {
        var dob = formSubmission["dob"];
        var now = new Date();
        var birthdate = dob.split("/");
        var born = new Date(birthdate[2], birthdate[1] - 1, birthdate[0]);
        age = getAge(born, now);
        if (age <= 18) {
            errorsArray.push("\n Date Of Birth should be greater than or equal to 18");
        }
    }

    var tnc = $('#tnc').is(":checked");
    if (!tnc) {
        errorsArray.push("\n Kindly accept the Terms and Conditions");
    }


    if (errorsArray.length > 0) {
        alert(errorsArray);
        errorsArray=[];
        return false;
    } else {
        return true;
    }
}


});

function validatePanCard(pancardNumber) {
  return $.ajax({

        url: "https://dbo.rsec.co.in/RelianceService/api/Registration/GetAndVerifiedPanNumberInfoByPanSiteChecking?", //production
        //url: " http://220.226.201.228/ProdRelianceService/api/Registration/GetAndVerifiedPanNumberInfoByPanSiteChecking?", //UAT
        type: "get",
         //send it through get method
         data: {
            PanNumber: pancardNumber
        }
    });    
}
