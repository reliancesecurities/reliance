var localStorage = window.localStorage;
localStorage.clear();
//$("#pan").attr('selected', 'selected');
var errorsArray = [];
getLocation();

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        alert("Geolocation is not supported by this browser.");
    }

}

function showPosition(position) {
    var location = "latitude =" + position.coords.latitude + " and longitude =" + position.coords.longitude;
    localStorage.setItem("location", location);
}

function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.")
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.")
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out.")
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.")
            break;
    }
}

$("#popupButton").click(function(e) {
    var otp = $("#otp").val();

    if (otp == localStorage.getItem("otp")) {
        //console.log("otp entered in popup is" + otp)
        //alert("otp matches");        
        sendFormDataToLMS(localStorage.getItem("formSubmission"));

    } else {
        alert("otp doesnt match");
    }

});


function successCallback(response)
{
    if (response == 0) {
        errorsArray.push("\n Enter Valid PAN Card Number");
    }
}

function validatePanCard(pancardNumber) {
    console.log(pancardNumber);
    $.ajax({
        url: "http://220.226.199.87/RelianceService/api/Registration/GetAndVerifiedPanNumberInfoByPanSiteChecking?", //production
        // url: " http://220.226.201.228/ProdRelianceService/api/Registration/GetAndVerifiedPanNumberInfoByPanSiteChecking?", //UAT
        //valid pan : dphpk7909k
        //invalid pan : aaaaa1111a
        type: "get", //send it through get method
         beforeSend: function(request) {
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  },
        data: {
            PanNumber: pancardNumber
        }
        ,
        success: function(response) {
            console.log(" Pan verification successful ");
            var valid = response[6];
            console.log(valid);
            return successCallback(valid);
        },
        error: function(jQXHR, textStatus, errorThrown) {
            console.log("An error occurred whilst trying to contact the server: " +
                jQXHR.status + " " + textStatus + " " + errorThrown);
        }
    });
}



$("#submitBtn").click(function(e) {

    e.preventDefault();

    var formSubmission = {
        name: $("#name").val(),
        email: $("#email").val(),
        mobile: $("#mobile").val(),
        pan: $("#pan").val(),
        dob: $("#dateOfBirth").val()
    }

    var allOkay = validateInput(formSubmission);

    if (allOkay) {
        var mobile = $("#mobile").val();

        formSubmission = JSON.stringify(formSubmission);
        localStorage.setItem("formSubmission", formSubmission);
        // sendSms(mobile);
        // $('#myModal').modal('show');
    }

});


function getAge(born, now) {
    var birthday = new Date(now.getFullYear(), born.getMonth(), born.getDate());
    if (now >= birthday)
        return now.getFullYear() - born.getFullYear();
    else
        return now.getFullYear() - born.getFullYear() - 1;
}

function sendSms(mobile) {
    var otp = getOtp();
    localStorage.setItem("otp", otp);

    $.ajax({
        url: "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?",
        type: "get", //send it through get method
        data: {
            jobname: 'RELSEC',
            feedid: '365430',
            password: 'jptpa',
            UserName: '8108292646',
            senderid: 'RELSEC',
            To: mobile,
            Text: 'Thank you for showing interest in opening a trading account with Reliance Securities. Your OTP is ' + otp
            //http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?jobname=RELSEC&feedid=365430&password=jptpa&UserName=8108292646&senderid=RELSEC&To=9702400424&Text=hacked
        },
        success: function(response) {
            //console.log( " success "+ otp + ' and mobile is '+mobile);
            console.log(" Successful Request Made on " +
                (new Date().toLocaleString()));
        },
        error: function(xhr, err) {
                console.log("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                console.log("Something went wrong when connecting on " +
                    (new Date().toLocaleString()));
            }
    });
}

function getOtp() {
    return Math.floor((Math.random() * 9999) + 1000);
}

function sendFormDataToLMS(formSubmission) {

    formSubmission = JSON.parse(formSubmission);

    var leadName = formSubmission["name"];
    var leadMobileNumber = formSubmission["mobile"];
    leadMobileNumber = parseInt(leadMobileNumber);
    var leadEmailId = formSubmission["email"];
    var location = localStorage.getItem("location");

    $.ajax({
        url: "http://lms.rsec.co.in/Leads/Service.svc/Rest/LeadCreation?",
        type: "get", //send it through get method
        data: {
            LeadSource: 'GGS_LP',
            UserName: 'test',
            Password: 'pass@123',
            Name: leadName,
            MobileNo: leadMobileNumber,
            EmailId: leadEmailId,
            Location: 'empty',
            Remarks: 'The location of lead is ' + location

        },
        success: function(response) {
            console.log(" Successful connection with server on " +
                (new Date().toLocaleString()));
            localStorage.setItem("confirmation-id", response);
            responseId = localStorage.getItem("confirmation-id");
            window.location = "thankyou.html?id=" + responseId;
        },
        error: function(jQXHR, textStatus, errorThrown) {
            console.log("An error occurred whilst trying to contact the server: " +
                jQXHR.status + " " + textStatus + " " + errorThrown + " on " +
                (new Date().toLocaleString()));
        }
    });
    var confirmationId = localStorage.getItem("confirmation-id");
    console.log(confirmationId);

}

function validateInput(formSubmission) {

    var nameRegex = /^[a-zA-Z ]{2,50}$/;
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var phoneRegexIndia = /^(\+\d{1,3}[- ]?)?\d{10}$/;
    var panRegex = /^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/;

    

    if (formSubmission["name"].length == 0 || formSubmission["name"] == "") {
        errorsArray.push("\n Enter Full Name");
    } else {
        if (!formSubmission["name"].match(nameRegex)) {
            errorsArray.push("\n Enter Correct Full Name");
        }
    }

    if (formSubmission["email"].length == 0 || formSubmission["email"] == "") {
        errorsArray.push("\n Enter Email Address");
    } else {
        if (!formSubmission["email"].match(emailRegex)) {
            errorsArray.push("\n Enter Valid Email Address");
        }
    }

    if (formSubmission["mobile"].length == 0 || formSubmission["mobile"] == "") {
        errorsArray.push("\n Enter Mobile Number");
    } else {
        if (!formSubmission["mobile"].match(phoneRegexIndia)) {
            errorsArray.push("\n Enter Valid Phone Number");
        }
    }

    if (formSubmission["pan"].length == 0 || formSubmission["pan"] == "") {
        errorsArray.push("\n Enter PAN Card Number");
    } else {
        if (!formSubmission["pan"].match(panRegex)) {
            errorsArray.push("\n Enter Valid PAN Card Number");
        } else {
            validatePanCard(formSubmission["pan"]);
        }
    }

    if (formSubmission["dob"].length == 0 || formSubmission["dob"] == "") {
        errorsArray.push("\n Enter Date Of Birth");
    } else {
        var dob = formSubmission["dob"];
        var now = new Date();
        var birthdate = dob.split("/");
        var born = new Date(birthdate[2], birthdate[1] - 1, birthdate[0]);
        age = getAge(born, now);
        if (age <= 18) {
            errorsArray.push("\n Date Of Birth should be greater than or equal to 18");
        }
    }

    var tnc = $('#tnc').is(":checked");
    if (!tnc) {
        errorsArray.push("\n Kindly accept the Terms and Conditions");
    }


    if (errorsArray.length > 0) {
        alert(errorsArray);
        return false;
    } else {
        return true;
    }
}
